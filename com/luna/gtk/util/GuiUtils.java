package com.luna.gtk.util;

import net.minecraft.src.FontRenderer;
import net.minecraft.src.Minecraft;
import net.minecraft.src.ScaledResolution;
import net.minecraft.src.Tessellator;
import org.lwjgl.opengl.GL11;

/**
 * OpenGL <3
 */
public final class GuiUtils {

    public static void color(int color) {
        float alpha = ((color >> 24) & 255) / 255.0F;
        float red = ((color >> 16) & 255) / 255.0F;
        float green = ((color >> 8) & 255) / 255.0F;
        float blue = (color & 255) / 255.0F;
        GL11.glColor4f(red, green, blue, alpha);
    }

    /**
     * Returns RRGGBBAA
     *
     * @param color
     * @return
     */
    public static float[] getColor(int color) {
        float alpha = ((color >> 24) & 255) / 255.0F;
        float red = ((color >> 16) & 255) / 255.0F;
        float green = ((color >> 8) & 255) / 255.0F;
        float blue = (color & 255) / 255.0F;
        return new float[] {
                red, green, blue, alpha
        };
    }

    /**
     * Returns the instance of the FontRenderer
     *
     * @return
     */
    public static FontRenderer getFontRenderer() {
        return Minecraft.getMinecraft().fontRenderer;
    }

    /**
     * SCALED width
     *
     * @return
     */
    public static int getWidth() {
        final ScaledResolution sr = getScaledResolution();
        return sr.getScaledWidth();
    }

    /**
     * SCALED height
     *
     * @return
     */
    public static int getHeight() {
        final ScaledResolution sr = getScaledResolution();
        return sr.getScaledHeight();
    }

    public static ScaledResolution getScaledResolution() {
        final ScaledResolution sr = new ScaledResolution(Minecraft.getMinecraft().gameSettings,
                Minecraft.getMinecraft().displayWidth, Minecraft.getMinecraft().displayHeight);
        return sr;
    }

	/*
	 * Various OpenGL methods
	 */

    public static void enableScissoring() {
        GL11.glEnable(GL11.GL_SCISSOR_TEST);
    }

    /**
     * @author Jonalu
     * @return Used for nifty things ;)
     * */
    public static void scissor(final float x, final float y, final float x2, final float y2) {
        final ScaledResolution sr = getScaledResolution();
        final int factor = sr.getScaleFactor();
        GL11.glScissor((int) (x * factor), (int) ((sr.getScaledHeight() - y2) * factor),
                (int) ((x2 - x) * factor), (int) ((y2 - y) * factor));
    }

    public static void disableScissoring() {
        GL11.glDisable(GL11.GL_SCISSOR_TEST);
    }

    public static void drawRect(double x, double y, double w, double h, int color) {
        Tessellator tess = Tessellator.instance;

        GL11.glEnable(GL11.GL_BLEND);
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

        color(color);

        tess.startDrawingQuads();
        tess.addVertex(x, y, 0);
        tess.addVertex(x, y+h, 0);
        tess.addVertex(x+w, y+h, 0);
        tess.addVertex(x+w, y, 0);
        tess.draw();

        GL11.glEnable(GL11.GL_TEXTURE_2D);
        GL11.glDisable(GL11.GL_BLEND);
    }

    public static void drawGradientRect(double x, double y, double w, double h, int color, int color2) {
        float var7 = (float)(color >> 24 & 255) / 255.0F;
        float var8 = (float)(color >> 16 & 255) / 255.0F;
        float var9 = (float)(color >> 8 & 255) / 255.0F;
        float var10 = (float)(color & 255) / 255.0F;
        float var11 = (float)(color2 >> 24 & 255) / 255.0F;
        float var12 = (float)(color2 >> 16 & 255) / 255.0F;
        float var13 = (float)(color2 >> 8 & 255) / 255.0F;
        float var14 = (float)(color2 & 255) / 255.0F;
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glDisable(GL11.GL_ALPHA_TEST);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glShadeModel(GL11.GL_SMOOTH);
        Tessellator var15 = Tessellator.instance;
        var15.startDrawingQuads();
        var15.setColorRGBA_F(var8, var9, var10, var7);
        var15.addVertex(x+w, y, 0);
        var15.addVertex(x, y, 0);
        var15.setColorRGBA_F(var12, var13, var14, var11);
        var15.addVertex(x, y+h, 0);
        var15.addVertex(x+w, y+h, 0);
        var15.draw();
        GL11.glShadeModel(GL11.GL_FLAT);
        GL11.glDisable(GL11.GL_BLEND);
        GL11.glEnable(GL11.GL_ALPHA_TEST);
        GL11.glEnable(GL11.GL_TEXTURE_2D);
    }

    public static void drawSideGradientRect(double x, double y, double w, double h, int color, int color2) {
        float var7 = (float)(color >> 24 & 255) / 255.0F;
        float var8 = (float)(color >> 16 & 255) / 255.0F;
        float var9 = (float)(color >> 8 & 255) / 255.0F;
        float var10 = (float)(color & 255) / 255.0F;
        float var11 = (float)(color2 >> 24 & 255) / 255.0F;
        float var12 = (float)(color2 >> 16 & 255) / 255.0F;
        float var13 = (float)(color2 >> 8 & 255) / 255.0F;
        float var14 = (float)(color2 & 255) / 255.0F;
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glDisable(GL11.GL_ALPHA_TEST);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glShadeModel(GL11.GL_SMOOTH);
        Tessellator var15 = Tessellator.instance;
        var15.startDrawingQuads();
        var15.setColorRGBA_F(var8, var9, var10, var7);
        var15.addVertex(x, y, 0);
        var15.addVertex(x, y+h, 0);
        var15.setColorRGBA_F(var12, var13, var14, var11);
        var15.addVertex(x+w, y+h, 0);
        var15.addVertex(x+w, y, 0);
        var15.draw();
        GL11.glShadeModel(GL11.GL_FLAT);
        GL11.glDisable(GL11.GL_BLEND);
        GL11.glEnable(GL11.GL_ALPHA_TEST);
        GL11.glEnable(GL11.GL_TEXTURE_2D);
    }

    public static void drawCircle(double x, double y, double r, int c) {
        // 0xAARRGGBB
        final float f = ((c >> 24) & 0xff) / 255F;
        final float f1 = ((c >> 16) & 0xff) / 255F;
        final float f2 = ((c >> 8) & 0xff) / 255F;
        final float f3 = (c & 0xff) / 255F;
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GL11.glEnable(GL11.GL_POLYGON_SMOOTH);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glHint(GL11.GL_POLYGON_SMOOTH_HINT, GL11.GL_NICEST);
        color(c);

        Tessellator tess = Tessellator.instance;

        tess.startDrawing(GL11.GL_POLYGON);
        for (int i = 0; i <= 360; i++) {
            final double x2 = Math.sin(((i * 3.141526D) / 180)) * r;
            final double y2 = Math.cos(((i * 3.141526D) / 180)) * r;
            tess.addVertex(x+x2, y+y2, 0);
        }
        tess.draw();
        GL11.glDisable(GL11.GL_POLYGON_SMOOTH);
        GL11.glEnable(GL11.GL_TEXTURE_2D);
        GL11.glDisable(GL11.GL_BLEND);
    }

    /**
     *
     * @param x - X Location
     * @param y - Y Location
     * @param rotation - How far to rotate the wedge
     * @param r - Radius
     * @param size - Size of the wedge in degrees
     * @param c - Color
     */
    public static void drawWedge(double x, double y, double rotation, double r, double size, int c) {
        // 0xAARRGGBB
        final float f = ((c >> 24) & 0xff) / 255F;
        final float f1 = ((c >> 16) & 0xff) / 255F;
        final float f2 = ((c >> 8) & 0xff) / 255F;
        final float f3 = (c & 0xff) / 255F;
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GL11.glEnable(GL11.GL_POLYGON_SMOOTH);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glHint(GL11.GL_POLYGON_SMOOTH_HINT, GL11.GL_NICEST);
        GL11.glTranslated(x, y, 0);
        GL11.glRotated(rotation, 0, 0, 1);
        color(c);

        Tessellator tess = Tessellator.instance;

        tess.startDrawing(GL11.GL_POLYGON);
        tess.addVertex(0, 0, 0);
        for (int i = 0; i <= size; i++) {
            final double x2 = Math.sin(((i * 3.141526D) / 180)) * r;
            final double y2 = Math.cos(((i * 3.141526D) / 180)) * r;
            tess.addVertex(x2, y2, 0);
        }
        tess.draw();

        GL11.glRotated(-rotation, 0, 0, 1);
        GL11.glTranslated(-x, -y, 0);
        GL11.glDisable(GL11.GL_POLYGON_SMOOTH);
        GL11.glEnable(GL11.GL_TEXTURE_2D);
        GL11.glDisable(GL11.GL_BLEND);
    }

    public static void drawRotatedString(String text, double x, double y, double rotation) {
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glEnable(GL11.GL_POLYGON_SMOOTH);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glHint(GL11.GL_POLYGON_SMOOTH_HINT, GL11.GL_NICEST);
        GL11.glTranslated(x, y, 0);
        GL11.glRotated(rotation, 0, 0, 1);

        Minecraft.getMinecraft().fontRenderer.drawString(text, (int) x, (int) y, 0xffffff);

        GL11.glRotated(-rotation, 0, 0, 1);
        GL11.glTranslated(-x, -y, 0);
        GL11.glDisable(GL11.GL_POLYGON_SMOOTH);
        GL11.glDisable(GL11.GL_BLEND);
    }

    public static void drawGradientCircle(double x, double y, double r, int c, int c2) {
        // 0xAARRGGBB
        final float f = ((c >> 24) & 0xff) / 255F;
        final float f1 = ((c >> 16) & 0xff) / 255F;
        final float f2 = ((c >> 8) & 0xff) / 255F;
        final float f3 = (c & 0xff) / 255F;
        final float g = ((c2 >> 24) & 0xff) / 255F;
        final float g1 = ((c2 >> 16) & 0xff) / 255F;
        final float g2 = ((c2 >> 8) & 0xff) / 255F;
        final float g3 = (c2 & 0xff) / 255F;
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GL11.glEnable(GL11.GL_POLYGON_SMOOTH);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glHint(GL11.GL_POLYGON_SMOOTH_HINT, GL11.GL_NICEST);
        GL11.glShadeModel(GL11.GL_SMOOTH);

        Tessellator tess = Tessellator.instance;

        tess.startDrawing(GL11.GL_POLYGON);
        tess.setColorRGBA_F(f1, f2, f3, f);
        tess.addVertex(x, y, 0);
        tess.setColorRGBA_F(g1, g2, g3, g);
        for (int i = 1; i <= 360; i++) {
            final double x2 = Math.sin(((i * 3.141526D) / 180)) * r;
            final double y2 = Math.cos(((i * 3.141526D) / 180)) * r;
            tess.addVertex(x+x2, y+y2, 0);
        }
        tess.draw();

        GL11.glShadeModel(GL11.GL_FLAT);
        GL11.glDisable(GL11.GL_POLYGON_SMOOTH);
        GL11.glEnable(GL11.GL_TEXTURE_2D);
        GL11.glDisable(GL11.GL_BLEND);
    }
}
