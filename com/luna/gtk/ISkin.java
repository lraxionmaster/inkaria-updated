package com.luna.gtk;

/**
 * Created with IntelliJ IDEA.
 * User: audrey
 * Date: 9/15/13
 * Time: 6:04 PM
 * To change this template use File | Settings | File Templates.
 */
public interface ISkin {
    void drawComponent(double x, double y, double w, double h, boolean flag);
}
