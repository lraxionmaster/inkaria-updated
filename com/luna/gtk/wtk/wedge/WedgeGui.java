package com.luna.gtk.wtk.wedge;

import com.luna.gtk.util.GuiUtils;
import net.minecraft.src.GuiScreen;
import net.minecraft.src.Minecraft;

/**
 * Created with IntelliJ IDEA.
 * User: audrey
 * Date: 9/23/13
 * Time: 5:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class WedgeGui extends Wedge {
    private GuiScreen screen;

    public WedgeGui(int wid, String text, GuiScreen screen) {
        super(wid, text);
        this.screen = screen;
    }

    @Override
    public void draw(double x, double y, double rotation, double size, double r, boolean flag) {
        GuiUtils.drawWedge(x, y, rotation, r, size, flag ? 0x7755ff55 : 0x77000000);

        this.drawWedgeString(getText().substring(0, getText().length() >= 9 ? 9 : getText().length()),
                x, y, rotation, size, r, flag);
    }

    @Override
    protected void clickMouse() {
        Minecraft.getMinecraft().displayGuiScreen(screen);
    }
}
