package com.luna.gtk.wtk.wedge;

import com.luna.gtk.util.GuiUtils;
import com.luna.lib.util.math.PolarCoords;
import net.minecraft.src.Minecraft;
import org.lwjgl.input.Mouse;

import java.awt.*;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created with IntelliJ IDEA.
 * User: audrey
 * Date: 9/19/13
 * Time: 4:18 PM
 * To change this template use File | Settings | File Templates.
 */
public class WedgeContainer {
    private Map<Wedge, Integer> wedgeMap;

    private double x, y, r;

    public WedgeContainer(double x, double y, double r) {
        wedgeMap = new ConcurrentHashMap<>();
        this.x = x;
        this.y = y;
        this.r = r;
    }

    public void addWedge(Wedge wedge) {
        if(wedgeMap.size() >= 8) {
            throw new IllegalStateException("Wedge map is at maximum capacity!");
        }

        wedge.setParent(this);

        synchronized (wedgeMap) {
            wedgeMap.put(wedge, wedgeMap.size());
        }
    }

    public void renderWedges() {
        //GuiUtils.drawCircle(x, y, r, 0xff000000);

        if(wedgeMap.size() > 8) {
            throw new IllegalStateException("Wedge map is at maximum capacity!");
        }

        double rot = (360D / (wedgeMap.size()));
        synchronized (wedgeMap) {
            for(Map.Entry<Wedge, Integer> e : wedgeMap.entrySet()) {
                e.getKey().draw(x, y, rot * e.getValue(), rot, r, mouseOver(e.getValue()));
            }
        }

        Point p = calculateMouseLocation();

        GuiUtils.drawCircle(p.x, p.y, 2, 0x77ffff00);
    }

    public void clickMouse(double x, double y, float partialTickTime) {
        synchronized (wedgeMap) {
            for(Map.Entry<Wedge, Integer> e : wedgeMap.entrySet()) {
                if(mouseOver(e.getValue())) {
                    e.getKey().clickMouse();
                }
            }
        }
    }

    /**
     * DarkStorm_'s utility method for calculating the location of the mouse.
     *
     * @return
     */
    private Point calculateMouseLocation() {
        final Minecraft minecraft = Minecraft.getMinecraft();
        int scale = minecraft.gameSettings.guiScale;
        if (scale == 0) {
            scale = 1000;
        }
        int scaleFactor = 0;
        while ((scaleFactor < scale) && ((minecraft.displayWidth / (scaleFactor + 1)) >= 320)
                && ((minecraft.displayHeight / (scaleFactor + 1)) >= 240)) {
            scaleFactor++;
        }
        return new Point(Mouse.getX() / scaleFactor, (minecraft.displayHeight / scaleFactor)
                - (Mouse.getY() / scaleFactor) - 1);
    }

    /**
     * Returns true if the mouse is hovering over wid
     *
     * @return
     */
    public boolean mouseOver(int wid) {
        final Point p = calculateMouseLocation();
        double dx = p.x - this.x;
        double dy = p.y - this.y;
        double rot = (360D / wedgeMap.size());

        if(calculateDistanceToPoint(p) <= this.r) {
            double distance = calculateDistanceToPoint(p);
            double dir = Math.atan2(dy, dx) * 180 / Math.PI;

            dir -= rot;

            if(dir <= 0) {
                dir += 360D;
            }
            if(dir >= 360) {
                dir = 360;
            }
            if(dir >= (rot*wid)) {
                if(dir < (rot*wid) + rot) {
                    return true;
                }
            }
        }
        return false;
    }

    protected double calculateDistanceToPoint(Point point) {
        double dx = point.x - this.x;
        double dy = point.y - this.y;

        return Math.sqrt((dx * dx) + (dy * dy));
    }

    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public Map<Wedge, Integer> getWedgeMap() {
        return wedgeMap;
    }

    public void clearWedges() {
        wedgeMap.clear();
    }

    /**
     * Centers the container on the screen
     */
    public void centerContainer() {
        this.x = (Minecraft.getMinecraft().displayWidth/4);
        this.y = (Minecraft.getMinecraft().displayHeight/4);
    }
}
