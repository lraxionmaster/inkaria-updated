package com.luna.gtk.wtk.wedge;

import com.luna.gtk.util.GuiUtils;
import com.luna.inkaria.logging.InkariaLogger;
import com.luna.lib.util.math.PolarCoords;
import net.minecraft.src.Minecraft;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: audrey
 * Date: 9/17/13
 * Time: 8:34 PM
 * To change this template use File | Settings | File Templates.
 */
public class TestWedge {

    private int x, y, r;

    public TestWedge() {
        this(100, 100);
    }

    public TestWedge(int x, int y) {
        this(x, y, 100);
    }

    public TestWedge(int x, int y, int r) {
        this.x = x;
        this.y = y;
        this.r = r;
    }

    public void draw() {
        GuiUtils.drawCircle(x, y, r, mouseOver() ? 0xffff0000 : 0xffffffff);

        GuiUtils.drawCircle(x + r/2, y - r/2, 3, 0xff000000);

        GL11.glPushMatrix();
        {
            for(int i = 0; i < 360; i += 60) {
                GL11.glTranslated(x, y, 0);
                GuiUtils.color(0xff000000);
                GL11.glRotated(i, 0, 0, 1);
                GL11.glDisable(GL11.GL_TEXTURE_2D);
                GL11.glEnable(GL11.GL_BLEND);
                GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
                GL11.glBegin(GL11.GL_LINES);
                GL11.glVertex2d(0, 0);
                GL11.glVertex2d(0, -r);
                GL11.glEnd();
                GL11.glDisable(GL11.GL_BLEND);
                GL11.glEnable(GL11.GL_TEXTURE_2D);
                GL11.glRotated(-i, 0, 0, 1);
                GL11.glTranslated(-x, -y, 0);
            }
        }
        GuiUtils.color(0xffffffff);
        GL11.glPopMatrix();
    }

    /**
     * DarkStorm_'s utility method for calculating the location of the mouse.
     *
     * @return
     */
    protected Point calculateMouseLocation() {
        final Minecraft minecraft = Minecraft.getMinecraft();
        int scale = minecraft.gameSettings.guiScale;
        if (scale == 0) {
            scale = 1000;
        }
        int scaleFactor = 0;
        while ((scaleFactor < scale) && ((minecraft.displayWidth / (scaleFactor + 1)) >= 320)
                && ((minecraft.displayHeight / (scaleFactor + 1)) >= 240)) {
            scaleFactor++;
        }
        return new Point(Mouse.getX() / scaleFactor, (minecraft.displayHeight / scaleFactor)
                - (Mouse.getY() / scaleFactor) - 1);
    }

    /**
     * Returns true if the mouse is hovering over this
     *
     * @return
     */
    public boolean mouseOver() {
        // Must pass in y as negative because of how MC does coords
        final Point p = calculateMouseLocation();
        if(calculateDistanceToPoint(p) <= this.r) {
            if(PolarCoords.cartesianToPolar(p.x, -p.y)[1] > 0) {
                if(PolarCoords.cartesianToPolar(p.x, -p.y)[1] < 60) {
                    return true;
                }
            }
        }
        return false;
    }

    protected double calculateDistanceToPoint(Point point) {
        Point p = point;
        p.x -= this.x;
        p.y -= this.y;
        double d = Math.abs(p.x * p.x);
        d += Math.abs(p.y * p.y);
        d = Math.sqrt(d);
        return d;
    }
}
