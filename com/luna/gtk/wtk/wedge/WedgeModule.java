package com.luna.gtk.wtk.wedge;

import com.luna.gtk.util.GuiUtils;
import com.luna.mtk.base.Module;
import net.minecraft.src.Minecraft;
import org.lwjgl.opengl.GL11;

import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: audrey
 * Date: 9/20/13
 * Time: 3:57 PM
 * To change this template use File | Settings | File Templates.
 */
public class WedgeModule extends Wedge {
    private final String text;
    private final Module module;

    Random randomGenerator = new Random();
    int red = randomGenerator.nextInt(255);
    int green = randomGenerator.nextInt(255);
    int blue = randomGenerator.nextInt(255);
    int color = (0xFF << 24) | ((red & 0xFF) << 16) | ((green & 0xFF) << 8) | ((blue & 0xFF) << 0);

    public WedgeModule(int wid, Module module) {
        super(wid, module.getName());
        this.module = module;
        text = module.getName();
    }

    @Override
    public void draw(double x, double y, double rotation, double size, double r, boolean flag) {
        GuiUtils.drawWedge(x, y, rotation, r, size, flag ? 0x7755ff55 : color);

        this.drawWedgeString(getText(), x, y, rotation, size, r, flag);

        /*final double x2 = (Math.sin((((-(rotation - (size / 2))) * 3.141526D) / 180)) * r);
        final double y2 = (Math.cos((((-(rotation - (size / 2))) * 3.141526D) / 180)) * r);

        final int stringSize = rotation > 180 && rotation <= 270 + size ? rotation == 180 + size ?
                (Minecraft.getMinecraft().fontRenderer.getStringWidth(text) / 2) + 2 :
                Minecraft.getMinecraft().fontRenderer.getStringWidth(text) :
                rotation == 180 ? Minecraft.getMinecraft().fontRenderer.getStringWidth(text) -
                        (Minecraft.getMinecraft().fontRenderer.getStringWidth(text) / 2) - 8 :
                        rotation == 0 ?
                                (Minecraft.getMinecraft().fontRenderer.getStringWidth(text) / 2) + 4 :
                                rotation == size ?
                                        (Minecraft.getMinecraft().fontRenderer.getStringWidth(text) / 2) - 7 : 0;

        final int stringSizeV = rotation == 180 ? -15 : rotation > 270 ? 20 : rotation <= 90 ? 20 :
                rotation < 180 && rotation > 90 ? -15 : rotation <= 270 && rotation > 180 ? -15 : 10;

        Minecraft.getMinecraft().fontRenderer.drawString(text, (int)(x+(x2 - (stringSize))), (int)(y+(y2-stringSizeV)),
                flag ? 0x55ff55 : 0xffffff);  */
    }

    @Override
    protected void clickMouse() {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
