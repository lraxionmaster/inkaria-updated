package com.luna.gtk.wtk.wedge;

import com.luna.gtk.util.GuiUtils;
import net.minecraft.src.Minecraft;
import org.lwjgl.opengl.GL11;

import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: audrey
 * Date: 9/19/13
 * Time: 4:20 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class Wedge {
    private final int wedgeID;

    private WedgeContainer parent;

    private String text;

    public Wedge(int wid, String text) {
        wedgeID = wid;
        this.text = text;
    }

    public final void onClick(double x, double y, int wedgeID) {
        if(wedgeID != this.wedgeID) {
            return;
        }
    }

    public abstract void draw(double x, double y, double rotation, double size, double r, boolean flag);/* {
        GuiUtils.drawWedge(x, y, rotation, r, size, flag ? 0xffff0000 : 0xff000000);
    }*/

    protected abstract void clickMouse();


    public void setParent(WedgeContainer newParent) {
        parent = newParent;
    }

    public WedgeContainer getParent() {
        return parent;
    }

    public String getText() {
        return text;
    }

    public void setText(String t) {
        text = t;
    }

    protected void drawWedgeString(String text, double x, double y, double rotation, double size, double r, boolean flag) {
        final double x2 = (Math.sin((((-(rotation - (size / 2))) * 3.141526D) / 180)) * r);
        final double y2 = (Math.cos((((-(rotation - (size / 2))) * 3.141526D) / 180)) * r);

        final int stringSize = rotation > 180 && rotation <= 270 + size ? rotation == 180 + size ?
                (Minecraft.getMinecraft().fontRenderer.getStringWidth(text) / 2) + 2 :
                Minecraft.getMinecraft().fontRenderer.getStringWidth(text) :
                rotation == 180 ? Minecraft.getMinecraft().fontRenderer.getStringWidth(text) -
                        (Minecraft.getMinecraft().fontRenderer.getStringWidth(text) / 2) - 8 :
                        rotation == 0 ?
                                (Minecraft.getMinecraft().fontRenderer.getStringWidth(text) / 2) + 4 :
                                rotation == size ?
                                        (Minecraft.getMinecraft().fontRenderer.getStringWidth(text) / 2) - 7 : 0;

        final int stringSizeV = rotation == 180 ? -15 : rotation > 270 ? 20 : rotation <= 90 ? 20 :
                rotation < 180 && rotation > 90 ? -15 : rotation <= 270 && rotation > 180 ? -15 : 10;

        Minecraft.getMinecraft().fontRenderer.drawString(text, (int)(x+(x2 - (stringSize))), (int)(y+(y2-stringSizeV)),
                0xffffff);
    }
}
