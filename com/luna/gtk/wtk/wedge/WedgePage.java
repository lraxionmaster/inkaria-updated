package com.luna.gtk.wtk.wedge;

import com.luna.gtk.util.GuiUtils;

import java.util.Random;

/**
 * Used for changing pages
 */
public class WedgePage extends Wedge {

    Random randomGenerator = new Random();
    int red = randomGenerator.nextInt(255);
    int green = randomGenerator.nextInt(255);
    int blue = randomGenerator.nextInt(255);
    int color = (0xFF << 24) | ((red & 0xFF) << 16) | ((green & 0xFF) << 8) | ((blue & 0xFF) << 0);

    public WedgePage(int wid) {
        super(wid, ".0.");
    }

    @Override
    public void draw(double x, double y, double rotation, double size, double r, boolean flag) {
        GuiUtils.drawWedge(x, y, rotation, r, size, flag ? 0x7755ff55 : 0x77000000);
    }

    @Override
    protected void clickMouse() {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
