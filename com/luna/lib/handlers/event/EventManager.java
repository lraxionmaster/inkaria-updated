package com.luna.lib.handlers.event;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.luna.lib.annotations.EventListener;
import com.luna.lib.event.EventBase;
import com.luna.lib.event.enums.EnumEventPriority;

/**
 * 
 * @author godshawk
 * 
 * @update Updated so that the annotations specify the priority
 * 
 */
public final class EventManager {
	/**
	 * Singleton
	 */
	private static volatile EventManager instance = new EventManager( );
	
	/**
	 * Map of classes that contain "event listener" methods.
	 */
	private volatile Map< Object, LinkedList< Method >> eventListeners;
	
	/**
	 * Since there will only ever be one instance of it, there is no reason for
	 * this to be public
	 */
	private EventManager( ) {
		eventListeners = new ConcurrentHashMap< Object, LinkedList< Method >>( );
		eventListeners.clear( );
	}
	
	/**
	 * Returns the singleton instance of the EventManager
	 * 
	 * @return
	 */
	public static final EventManager getInstance( ) {
		return instance;
	}
	
	/**
	 * Adds a listener.
	 * 
	 * @param o
	 */
	public final void addListener( final Object o ) {
        final Method[ ] declared = o.getClass( ).getDeclaredMethods( );
        final LinkedList< Method > listeners = new LinkedList< Method >( );

        for ( final Method e : declared ) {
            if ( e.isAnnotationPresent( EventListener.class ) ) {
                listeners.add( e );
            }
        }

        synchronized ( eventListeners ) {
            eventListeners.put( o, listeners );
        }
	}
	
	/**
	 * Removes a listener.
	 * 
	 * @param o
	 */
	public final void removeListener( final Object o ) {
		synchronized ( eventListeners ) {
			for ( final Map.Entry< Object, LinkedList< Method >> e : eventListeners.entrySet( ) ) {
				if ( e.getKey( ).equals( o ) ) {
					eventListeners.remove( e.getKey( ) );
				}
			}
		}
	}
	
	/**
	 * Returns the map of listeners
	 * 
	 * @return
	 */
	public final Map< Object, LinkedList< Method >> getEventListeners( ) {
		synchronized ( eventListeners ) {
			return eventListeners;
		}
	}
	
	/**
	 * Sends out an event
	 * 
	 * @param event
	 */
	public final void fire( final EventBase event ) {
		// Synchronize synchronize synchronize!
		synchronized ( eventListeners ) {
			// Iterate through EventPriorities
			for ( final EnumEventPriority pr : EnumEventPriority.values( ) ) {
				// Iterate through map of listeners
				for ( final Map.Entry< Object, LinkedList< Method >> e : getEventListeners( ).entrySet( ) ) {
					// Get the list of event-listening methods in the class
					final List< Method > f = e.getValue( );
					// Iterate through methods
					for ( final Method g : f ) {
						// For each String in the annotations parameters
						// If the String is equal to the event's name
						final EventListener h = g.getAnnotation( EventListener.class );
						if ( h.event( ).equals( event.getClass( ) ) ) {
							// Check for the priority
							if ( !h.priority( ).equals( pr ) ) {
								continue;
							}
							// Make the method accessible
							g.setAccessible( true );
							// Attempt to invoke it, log the error if it
							// fails
							try {
								// If there's no parameters for the method,
								// just invoke it.
								if ( g.getParameterAnnotations( ).length == 0 ) {
									g.invoke( e.getKey( ) );
								}
								// If the method DOES have a parameter,
								// invoke it and pass in the event as the
								// parameter.
								if ( g.getParameterAnnotations( ).length == 1 ) {
									g.invoke( e.getKey( ), event );
								}
							} catch ( final IllegalAccessException | InvocationTargetException e1 ) {
								// Error logging
								e1.printStackTrace( );
							}
						}
					}
				}
			}
		}
	}
}
