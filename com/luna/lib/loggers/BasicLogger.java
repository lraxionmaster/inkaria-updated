package com.luna.lib.loggers;

import com.luna.lib.loggers.enums.EnumLogType;

public class BasicLogger extends AbstractLogger {
	
	private static final BasicLogger instance = new BasicLogger( );
	
	@Override
	public void log( final EnumLogType level, final Object data ) {
		System.out.println( String.format( "[%s] %s", level.getName( ), data.toString( ) ) );
	}
	
	public static BasicLogger getInstance( ) {
		return instance;
	}
}
