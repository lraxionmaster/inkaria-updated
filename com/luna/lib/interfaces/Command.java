package com.luna.lib.interfaces;

/**
 * Created with IntelliJ IDEA.
 * User: audrey
 * Date: 9/13/13
 * Time: 3:57 PM
 * To change this template use File | Settings | File Templates.
 */
public interface Command {
    public void onCommand(String[] args);
}
