package com.luna.lib.interfaces;

/**
 * Classes that implement this can be ticked
 */
public interface Tickable {
	public abstract void tick( );
}
