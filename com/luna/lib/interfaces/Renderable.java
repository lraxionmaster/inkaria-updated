package com.luna.lib.interfaces;

/**
 * Classes that implement this can be rendered
 */
public interface Renderable {
	public abstract void render( );
}
