package com.luna.tki;

import com.luna.ctk.ConsoleToolkit;
import com.luna.inkaria.logging.InkariaLogger;
import com.luna.lib.loggers.enums.EnumLogType;
import com.luna.lib.reflection.ClassEnumerator;
import com.luna.mtk.ModuleToolkit;
import com.luna.mtk.annotations.Loadable;
import com.luna.mtk.base.Module;

import java.util.ArrayList;
import java.util.List;

/**
 * Classes in this package are used for 'integration' of the different toolkits
 */
public final class ToolkitIntegration {
    public static void loadModuleCommands() {
        InkariaLogger.getInstance().log(EnumLogType.REFLECTION, "Adding missing Module commands...");
        for(Module e : getModulesFromPackage(ModuleToolkit.getClassToLoadFrom())) {
            ConsoleToolkit.getInstance().addCommand(new ModuleBaseCommand(e));
        }
    }

    /**
     * Parses the class code source to get modules from the retrieved classes
     *
     * @param classe
     *         class to get code source for
     * @return module array from package
     */
    private static Module[] getModulesFromPackage(Class<?> classe)
    {
        final List<Module> modules = new ArrayList<Module>();
        final Class[] classes = ClassEnumerator.getInstance().getClassesFromPackage(classe);
        for (Class<?> c : classes)
        {
            if (Module.class.isAssignableFrom(c) &&
                    !c.equals(Module.class))
            {
                if(!c.isAnnotationPresent(Loadable.class)) {
                    InkariaLogger.getInstance().log(EnumLogType.REFLECTION, String.format("Skipping %s because it is not marked as loadable", c.getName()));
                    continue;
                }
                try
                { modules.add((Module) c.newInstance()); }
                catch (InstantiationException e)
                { e.printStackTrace(); }
                catch (IllegalAccessException e)
                { e.printStackTrace(); }
            }
        }
        return modules.toArray(new Module[modules.size()]);
    }
}
