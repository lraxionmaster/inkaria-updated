package com.luna.tki;

import com.luna.ctk.base.BaseCommand;
import com.luna.mtk.base.Module;

/**
 * Created with IntelliJ IDEA.
 * User: godshawk
 * Date: 9/9/13
 * Time: 6:46 PM
 * To change this template use File | Settings | File Templates.
 */
public class ModuleBaseCommand extends BaseCommand {
    private Module module;

    public ModuleBaseCommand(Module m) {
        super(m.getName(), m.getDesc(), m.getDesc());
        this.module = m;
    }

    @Override
    public void onCommand(String[] args) {
        module.onCommand(args);
    }

    public ModuleBaseCommand setNewSyntax(String syntax) {
        setSyntax(syntax);
        return this;
    }
}
