package com.luna.mtk.events;

import com.luna.lib.event.EventBase;

/**
 * Created with IntelliJ IDEA.
 * User: godshawk
 * Date: 9/8/13
 * Time: 3:10 PM
 * To change this template use File | Settings | File Templates.
 */
public final class EventKey extends EventBase {
    private final int key;

    /**
     * Constructor
     *
     * @param source
     */
    public EventKey(Object source, int key) {
        super(source);
        this.key = key;
    }

    public final int getKey() {
        return key;
    }
}
