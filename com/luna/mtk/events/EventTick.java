package com.luna.mtk.events;

import com.luna.lib.event.EventBase;

/**
 * Created with IntelliJ IDEA.
 * User: godshawk
 * Date: 9/8/13
 * Time: 2:14 PM
 * To change this template use File | Settings | File Templates.
 */
public final class EventTick extends EventBase {
    /**
     * Constructor
     *
     * @param source
     */
    public EventTick(Object source) {
        super(source);
    }
}
