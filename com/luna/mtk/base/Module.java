package com.luna.mtk.base;

import com.luna.lib.handlers.event.EventManager;
import com.luna.lib.interfaces.Command;
import com.luna.lib.interfaces.Renderable;
import com.luna.lib.interfaces.Tickable;
import com.luna.mtk.base.enums.EnumModuleType;
import net.minecraft.src.EntityClientPlayerMP;
import net.minecraft.src.Minecraft;
import net.minecraft.src.PotionEffect;
import net.minecraft.src.WorldClient;

/**
 * Created with IntelliJ IDEA.
 * User: godshawk
 * Date: 9/8/13
 * Time: 12:41 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class Module implements Tickable, Renderable, Command {
    private int key;

    private String name, desc, syntax;

    private EnumModuleType type;

    // Don't HAVE to initialize this, but why not?
    private boolean state = false;

    public Module(String name, String desc, EnumModuleType type) {
        this(name, desc, -1, type);
    }

    public Module(String name, String desc, int key, EnumModuleType type) {
        this.name = name;
        this.desc = desc;
        this.key = key;
        this.type = type;
    }

    public void toggle() {
        state = !state;
        if(state) {
            EventManager.getInstance().addListener(this);
            onEnable();
        } else {
            EventManager.getInstance().removeListener(this);
            onDisable();
        }
    }

    @Override
    public void onCommand(String[] args) {
        toggle();
    }

    public abstract void tick();

    public abstract void render();

    protected void onEnable() {
    }

    protected void onDisable() {
    }

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int k) {
        key = k;
    }

    public EnumModuleType getType() {
        return type;
    }

    public boolean getActive() {
        return state;
    }

    protected static Minecraft getMinecraft() {
        return Minecraft.getMinecraft();
    }

    protected static EntityClientPlayerMP getPlayer() {
        return getMinecraft().thePlayer;
    }

    protected static WorldClient getWorld() {
        return getMinecraft().theWorld;
    }

    protected static void addPotionEffect(PotionEffect effect) {
        getPlayer().addPotionEffect(effect);
    }

    protected static void removePotionEffect(int id) {
        getPlayer().removePotionEffect(id);
    }
}
