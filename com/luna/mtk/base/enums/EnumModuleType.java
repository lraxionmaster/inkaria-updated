package com.luna.mtk.base.enums;

/**
 * Created with IntelliJ IDEA.
 * User: godshawk
 * Date: 9/8/13
 * Time: 1:29 PM
 * To change this template use File | Settings | File Templates.
 */
public enum EnumModuleType {
    PLAYER, RENDER, WORLD, COMBAT, AURA;
}
