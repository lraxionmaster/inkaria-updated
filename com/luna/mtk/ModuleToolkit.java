package com.luna.mtk;

import com.luna.inkaria.logging.InkariaLogger;
import com.luna.lib.loggers.enums.EnumLogType;
import com.luna.lib.reflection.ClassEnumerator;
import com.luna.mtk.annotations.Loadable;
import com.luna.mtk.base.Module;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: godshawk
 * Date: 9/8/13
 * Time: 12:43 PM
 * To change this template use File | Settings | File Templates.
 */
public final class ModuleToolkit {
    private static volatile ModuleToolkit instance;

    private Set<Module> modules;

    private static Class<? extends Module> classToLoadFrom;

    private ModuleToolkit() {
        InkariaLogger.getInstance().log(EnumLogType.SCAN, "Scanning minecraft.jar for built-in modules and loading them...");
        modules = new LinkedHashSet<>(Arrays.asList(this.getModulesFromPackage(classToLoadFrom)));
        InkariaLogger.getInstance().log(EnumLogType.SUCCESS, String.format("Modules loaded: %d", modules.size()));
    }

    public static final ModuleToolkit getInstance() {
        if(instance  == null) {
            instance = new ModuleToolkit();
        }
        return instance;
    }

    public static void setClassToLoadFrom(Class<? extends Module> e) {
        classToLoadFrom = e;
    }

    public static Class<? extends Module> getClassToLoadFrom() {
        return classToLoadFrom;
    }

    public Set<Module> getModules() {
        return modules;
    }

    public Module getModuleByClass(Class clazz) {
        for(Module e : modules) {
            if(e.getClass().equals(clazz)) {
                return e;
            }
        }
        return null;
    }

    /**
     * Parses the class code source to get modules from the retrieved classes
     *
     * @param classe
     *         class to get code source for
     * @return module array from package
     */
    private Module[] getModulesFromPackage(Class<?> classe)
    {
        final List<Module> modules = new ArrayList<Module>();
        final Class[] classes = ClassEnumerator.getInstance().getClassesFromPackage(classe);
        for (Class<?> c : classes)
        {
            if (Module.class.isAssignableFrom(c) &&
                    !c.equals(Module.class))
            {
                if(!c.isAnnotationPresent(Loadable.class)) {
                    InkariaLogger.getInstance().log(EnumLogType.REFLECTION, String.format("Skipping %s because it is not marked as loadable", c.getName()));
                    continue;
                }
                InkariaLogger.getInstance().log(EnumLogType.REFLECTION, String.format("Instantiating %s...", c.getName()));
                try
                { modules.add((Module) c.newInstance()); }
                catch (InstantiationException e)
                { e.printStackTrace(); }
                catch (IllegalAccessException e)
                { e.printStackTrace(); }
            }
        }
        return modules.toArray(new Module[modules.size()]);
    }
}
