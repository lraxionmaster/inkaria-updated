package com.luna.mtk;

import com.luna.inkaria.logging.InkariaLogger;
import com.luna.lib.annotations.EventListener;
import com.luna.lib.event.enums.EnumEventPriority;
import com.luna.lib.handlers.event.EventManager;
import com.luna.mtk.base.Module;
import com.luna.mtk.events.EventKey;
import net.minecraft.src.Minecraft;

/**
 * Created with IntelliJ IDEA.
 * User: godshawk
 * Date: 9/8/13
 * Time: 3:08 PM
 * To change this template use File | Settings | File Templates.
 */
public final class KeyboardHandler {
    private static volatile KeyboardHandler instance;

    private KeyboardHandler() {
        EventManager.getInstance().addListener(this);
    }

    public static KeyboardHandler getInstance() {
        if(instance == null) {
            instance = new KeyboardHandler();
        }
        return instance;
    }

    /**
     * Listens to keypresses
     */
    @EventListener(event = EventKey.class, priority = EnumEventPriority.HIGHEST)
    public void listen(EventKey keyEvent) {
        if(Minecraft.getMinecraft().thePlayer == null || Minecraft.getMinecraft().theWorld == null) {
            return;
        }

        InkariaLogger.getInstance().log(String.format("Key pressed: %d", keyEvent.getKey()));
        for(Module e : ModuleToolkit.getInstance().getModules()) {
            if(e.getKey() == keyEvent.getKey()) {
                InkariaLogger.getInstance().log(String.format("Module found: %s", e.getName()));
                e.toggle();
            }
        }
    }
}
