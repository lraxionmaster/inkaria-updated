package com.luna.ctk;

import com.luna.ctk.base.BaseCommand;
import com.luna.inkaria.logging.InkariaLogger;
import com.luna.tki.ModuleBaseCommand;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * This will eventually be like MTK; ie Reflectively search for commands, that's just not a priority ATM
 */
public final class ConsoleToolkit {
    private static volatile ConsoleToolkit instance;

    private Set<BaseCommand> baseCommands;

    private ConsoleToolkit() {
        baseCommands = new LinkedHashSet<>();
    }

    public static final ConsoleToolkit getInstance() {
        if(instance == null) {
            instance = new ConsoleToolkit();
        }
        return instance;
    }

    public void addCommand(BaseCommand baseCommand) {
        String isModule = (baseCommand instanceof ModuleBaseCommand) ? "module " : "";
        InkariaLogger.getInstance().log(String.format("Adding %scommand: %s", isModule, baseCommand.getName()));
        baseCommands.add(baseCommand);
    }
}
