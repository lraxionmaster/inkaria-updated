package com.luna.ctk.base;

import com.luna.gtk.util.ChatColor;
import com.luna.inkaria.logging.ChatLogger;
import com.luna.lib.interfaces.Command;

/**
 * Base of all commands
 */
public abstract class BaseCommand implements Command {
    private String name, desc, syntax;

    public BaseCommand(String name, String desc, String syntax) {
        this.name = name;
        this.desc = desc;
        this.syntax = syntax;
    }

    /**
     * Prints the help to the chat
     */
    public final void getHelp() {
        ChatLogger.getInstance().log("----------------");
        ChatLogger.getInstance().log(String.format("%sName%s: %s%s%s", ChatColor.GOLD, ChatColor.RESET, ChatColor.GREEN, name, ChatColor.RESET));
        ChatLogger.getInstance().log(String.format("%sSyntax%s: %s%s%s", ChatColor.GOLD, ChatColor.RESET, ChatColor.GREEN, syntax, ChatColor.RESET));
        ChatLogger.getInstance().log(String.format("%sDescription%s: %s%s%s", ChatColor.GOLD, ChatColor.RESET, ChatColor.GREEN, desc, ChatColor.RESET));
        ChatLogger.getInstance().log("----------------");
    }

    public String getName() {
        return name;
    }

    protected void setSyntax(String syntax) {
        this.syntax = syntax;
    }
}
