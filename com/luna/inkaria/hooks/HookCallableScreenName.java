package com.luna.inkaria.hooks;

import net.minecraft.src.EntityRenderer;

import java.util.concurrent.Callable;

/**
 * Created with IntelliJ IDEA.
 * User: godshawk
 * Date: 9/8/13
 * Time: 6:42 PM
 * To change this template use File | Settings | File Templates.
 */
public class HookCallableScreenName implements Callable {
    final HookEntityRenderer entityRender;

    HookCallableScreenName(HookEntityRenderer par1EntityRenderer)
    {
        this.entityRender = par1EntityRenderer;
    }

    public String callScreenName()
    {
        return HookEntityRenderer.getRendererMinecraft(this.entityRender).currentScreen.getClass().getCanonicalName();
    }

    public Object call()
    {
        return this.callScreenName();
    }
}
