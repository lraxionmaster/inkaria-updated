package com.luna.inkaria.hooks;

import net.minecraft.src.EntityRenderer;
import net.minecraft.src.ScaledResolution;

import java.util.concurrent.Callable;

/**
 * Created with IntelliJ IDEA.
 * User: godshawk
 * Date: 9/8/13
 * Time: 6:41 PM
 * To change this template use File | Settings | File Templates.
 */
public class HookCallableScreenSize implements Callable {
    final ScaledResolution theScaledResolution;

    final HookEntityRenderer theEntityRenderer;

    HookCallableScreenSize(HookEntityRenderer par1EntityRenderer, ScaledResolution par2ScaledResolution)
    {
        this.theEntityRenderer = par1EntityRenderer;
        this.theScaledResolution = par2ScaledResolution;
    }

    public String callScreenSize()
    {
        return String.format("Scaled: (%d, %d). Absolute: (%d, %d). Scale factor of %d", new Object[] {Integer.valueOf(this.theScaledResolution.getScaledWidth()), Integer.valueOf(this.theScaledResolution.getScaledHeight()), Integer.valueOf(HookEntityRenderer.getRendererMinecraft(this.theEntityRenderer).displayWidth), Integer.valueOf(HookEntityRenderer.getRendererMinecraft(this.theEntityRenderer).displayHeight), Integer.valueOf(this.theScaledResolution.getScaleFactor())});
    }

    public Object call()
    {
        return this.callScreenSize();
    }
}
