package com.luna.inkaria.hooks;

import net.minecraft.src.EntityRenderer;
import org.lwjgl.input.Mouse;

import java.util.concurrent.Callable;

/**
 * Created with IntelliJ IDEA.
 * User: godshawk
 * Date: 9/8/13
 * Time: 6:40 PM
 * To change this template use File | Settings | File Templates.
 */
public class HookCallableMouseLocation implements Callable {
    final int field_90026_a;

    final int field_90024_b;

    final EntityRenderer theEntityRenderer;

    HookCallableMouseLocation(EntityRenderer par1EntityRenderer, int par2, int par3)
    {
        this.theEntityRenderer = par1EntityRenderer;
        this.field_90026_a = par2;
        this.field_90024_b = par3;
    }

    public String callMouseLocation()
    {
        return String.format("Scaled: (%d, %d). Absolute: (%d, %d)", new Object[] {Integer.valueOf(this.field_90026_a), Integer.valueOf(this.field_90024_b), Integer.valueOf(Mouse.getX()), Integer.valueOf(Mouse.getY())});
    }

    public Object call()
    {
        return this.callMouseLocation();
    }
}
