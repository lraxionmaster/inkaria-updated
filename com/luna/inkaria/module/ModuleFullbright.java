package com.luna.inkaria.module;

import com.luna.mtk.events.EventTick;
import com.luna.lib.annotations.EventListener;
import com.luna.mtk.annotations.Loadable;
import com.luna.mtk.base.Module;
import com.luna.mtk.base.enums.EnumModuleType;
import net.minecraft.src.Potion;
import net.minecraft.src.PotionEffect;
import org.lwjgl.input.Keyboard;

@Loadable
public class ModuleFullbright extends Module {
    public ModuleFullbright() {
        super("Fullbright", "Lets you see in the dark", Keyboard.KEY_F, EnumModuleType.WORLD);
    }

    @Override
    @EventListener(event = EventTick.class)
    public void tick() {
         addPotionEffect(new PotionEffect(Potion.nightVision.getId(), 99999999, 255, true));
    }

    @Override
    public void render() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void onDisable() {
        removePotionEffect(Potion.nightVision.getId());
    }
}
