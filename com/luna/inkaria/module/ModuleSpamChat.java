package com.luna.inkaria.module;

import com.luna.mtk.events.EventTick;
import com.luna.lib.annotations.EventListener;
import com.luna.mtk.annotations.Loadable;
import com.luna.mtk.base.Module;
import com.luna.mtk.base.enums.EnumModuleType;
import org.lwjgl.input.Keyboard;

import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: godshawk
 * Date: 9/8/13
 * Time: 5:01 PM
 * To change this template use File | Settings | File Templates.
 */
@Loadable
public class ModuleSpamChat extends Module {
    public ModuleSpamChat() {
        super("SpamChat", "Spams your chat with crap", Keyboard.KEY_A, EnumModuleType.RENDER);
    }

    @Override
    @EventListener(event = EventTick.class)
    public void tick() {
        getPlayer().addChatMessage(Integer.toString(new Random().nextInt()));
    }

    @Override
    public void render() {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
