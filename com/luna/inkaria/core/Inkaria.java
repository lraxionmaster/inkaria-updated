package com.luna.inkaria.core;

import com.luna.ctk.ConsoleToolkit;
import com.luna.inkaria.logging.InkariaLogger;
import com.luna.inkaria.module.ModuleFullbright;
import com.luna.mtk.KeyboardHandler;
import com.luna.mtk.ModuleToolkit;
import com.luna.tki.ToolkitIntegration;

/**
 * Base classes edited:
 *
 * Minecraft.java
 *
 * Testing:
 * GuiMainMenu.java
 * GuiIngame.java
 */
public final class Inkaria {

    private static volatile Inkaria instance;

    private final String name = "Inkaria";
    private final String version = "2";
    private final String releaseID = "Seraphim";

    private Inkaria() {

        InkariaLogger.getInstance().log("##### #   # #  # ##### ##### ##### #####");
        InkariaLogger.getInstance().log("  #   ##  # # #  #   # #   #   #   #   #");
        InkariaLogger.getInstance().log("  #   # # # ##   ##### #####   #   #####");
        InkariaLogger.getInstance().log("  #   #  ## # #  #   # #  #    #   #   #");
        InkariaLogger.getInstance().log("##### #   # #  # #   # #   # ##### #   #");

        InkariaLogger.getInstance().log(String.format("Loading %s...", getClientName()));
        InkariaLogger.getInstance().log("This is " + getClientName());

        setPreLoadArgs();
        loadSingletons();

        InkariaLogger.getInstance().log("Inkaria loaded");
    }

    public static Inkaria getInstance() {
        if(instance == null) {
            instance = new Inkaria();
        }
        return instance;
    }

    private void loadSingletons() {
        InkariaLogger.getInstance().log("Creating singletons...");
        ModuleToolkit.getInstance();
        KeyboardHandler.getInstance();
        ConsoleToolkit.getInstance();
        ToolkitIntegration.loadModuleCommands();
    }

    private void setPreLoadArgs() {
        InkariaLogger.getInstance().log("Setting preload variables");
        ModuleToolkit.setClassToLoadFrom(ModuleFullbright.class);
    }

    public String getClientName() {
        return String.format("%s %s \"%s\"", name, version, releaseID);
    }
}
