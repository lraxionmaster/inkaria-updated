package com.luna.inkaria.logging;

import com.luna.lib.loggers.BasicLogger;
import com.luna.lib.loggers.enums.EnumLogType;

/**
 * Created with IntelliJ IDEA.
 * User: godshawk
 * Date: 9/8/13
 * Time: 12:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class InkariaLogger extends BasicLogger {

    private static final InkariaLogger instance = new InkariaLogger();

    @Override
    public void log(final EnumLogType level, final Object data) {
        System.out.println(String.format("[Inkaria] [%s] %s", level.getName(), data.toString()));
    }

    public static final InkariaLogger getInstance() {
        return instance;
    }

}
