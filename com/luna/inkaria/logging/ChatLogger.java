package com.luna.inkaria.logging;

import com.luna.gtk.util.ChatColor;
import com.luna.lib.loggers.AbstractLogger;
import com.luna.lib.loggers.enums.EnumLogType;
import net.minecraft.src.Minecraft;

import java.util.HashMap;
import java.util.Map;

/**
 * Used for logging to the chat in Minecraft
 */
public final class ChatLogger extends AbstractLogger {
    private static final ChatLogger instance = new ChatLogger();

    /**
     * Only ever used here; that's why it was removed from EnumLogType
     */
    private final Map<EnumLogType, String> colors = new HashMap<EnumLogType, String>();

    {
        colors.put(EnumLogType.INFO, ChatColor.GOLD);
        colors.put(EnumLogType.WARNING, ChatColor.RED);
        colors.put(EnumLogType.FATAL, ChatColor.DARK_RED);
        colors.put(EnumLogType.TRACE, ChatColor.YELLOW);
        colors.put(EnumLogType.HOOK, ChatColor.GREY);
        colors.put(EnumLogType.SCAN, ChatColor.WHITE);
        colors.put(EnumLogType.DEBUG, ChatColor.AQUA);
        colors.put(EnumLogType.SUCCESS, ChatColor.GREEN);
        colors.put(EnumLogType.IO, ChatColor.DARK_BLUE);
        colors.put(EnumLogType.REFLECTION, ChatColor.BLUE);
    }

    @Override
    public void log(final EnumLogType level, final Object data) {

        try {
            Minecraft.getMinecraft().thePlayer.addChatMessage(String.format("[%s%s%s] %s",
                    getColor(level), level.getName(), ChatColor.RESET, data.toString()));
        } catch (final NullPointerException e) {
            e.printStackTrace();
        }
    }

    public static ChatLogger getInstance() {
        return instance;
    }

    private String getColor(final EnumLogType level) {
        for (final Map.Entry<EnumLogType, String> e : colors.entrySet()) {
            if (e.getKey().equals(level)) {
                return e.getValue();
            }
        }
        return ChatColor.RESET;
    }
}
